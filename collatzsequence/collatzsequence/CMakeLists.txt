cmake_minimum_required(VERSION 2.6)

project (collatzsequence CXX)

set(COLLATZSEQUENCE_HEADERS collatzsequence.h)

set(PROJECT_SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/src)
set(COLLATZSEQUENCE_CODEGEN_DIR "${PROJECT_BINARY_DIR}/codegen")

PREPEND(COLLATZSEQUENCE_HEADERS_PATHS ${PROJECT_SOURCE_DIR} ${COLLATZSEQUENCE_HEADERS})

CODEGEN_FILES(COLLATZSEQUENCE_CODEGEN_SOURCES ${COLLATZSEQUENCE_CODEGEN_DIR} ${COLLATZSEQUENCE_HEADERS})

add_custom_command(OUTPUT ${COLLATZSEQUENCE_CODEGEN_SOURCES}
    COMMAND ${NGREST_BIN_PATH}ngrestcg -i "${PROJECT_SOURCE_DIR}" -o ${COLLATZSEQUENCE_CODEGEN_DIR} -t service ${COLLATZSEQUENCE_HEADERS}
    DEPENDS ${COLLATZSEQUENCE_HEADERS_PATHS}
)

file(GLOB COLLATZSEQUENCE_SOURCES ${PROJECT_SOURCE_DIR}/*.cpp)

list(APPEND COLLATZSEQUENCE_SOURCES ${COLLATZSEQUENCE_CODEGEN_SOURCES})

include_directories(${PROJECT_SOURCE_DIR} $ENV{NGREST_EXT_INCLUDES})

add_library(collatzsequence MODULE ${COLLATZSEQUENCE_SOURCES})

set_target_properties(collatzsequence PROPERTIES PREFIX "")
set_target_properties(collatzsequence PROPERTIES
    LIBRARY_OUTPUT_DIRECTORY "${PROJECT_SERVICES_DIR}"
)

target_link_libraries(collatzsequence ngrestutils ngrestcommon ngrestjson ngrestengine $ENV{NGREST_EXT_LIBS})
