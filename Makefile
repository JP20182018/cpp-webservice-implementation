#Make
all: help

iniciarWebservice:
	-@echo "Iniciando WebService C++. Presione CTRL+C para cancelar."	
	-cd	collatzsequence;ngrest

crearWebservice:
	-ngrest create collatzsequence

generarAmbiente:
	-@echo "Instalando ngrest: Componente de WebServices C++"	
	wget -qO- http://bit.ly/ngrest | bash
	sudo apt install curl


testearAmbiente:
	-@echo "Unit Test con Postman, de WebServices C++..."
	curl -H 'Content-type: application/json'	-X GET --data '{"n":"43"}' \
	http://localhost:9098/collatzsequence/UnitTestSequenciaCollatz	
	
help:
	-@echo "MAKE generarAmbiente: Genera el ambiente necesario para ejecutar los webservices."
	-@echo "MAKE iniciarWebservice: Inicia el webservice de pruebas."
	-@echo "MAKE testearAmbiente: Realiza pruebas unitarias a los webservices ya desplegados."
	-@echo "MAKE help: Este Makefile"
	
