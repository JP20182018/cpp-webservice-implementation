# README #

Implementación de Web Service C++ con sequencia collatz, para Ubuntu 18.04 x64 bits. Incluye tests unitarios.

Instrucciones:

A continuación se adjunta un Makefile con los comandos que se deben ejecutar para generar el ambiente.

#Terminal 1:
Se debe abrir un terminal, luego navegar hasta el directorio del repositorio actual, y ejecutar:

>make generarAmbiente
>make iniciarWebservice

Este terminal se encuentra ejecutando el Webservice. Se debe abrir un segundo terminal para realizar el unit test del webservice.
No se debe cerrar mientras se realiza el unit test.

#Terminal 2:
Se debe abrir un terminal, luego navegar hasta el directorio del repositorio actual, y ejecutar:
>make testearAmbiente

CURL se encargará de realizar el unittest con la secuencia Collatz. Si existe un error en el unit test, Makefile retornará error en el el UnitTest.
De lo contrario, se verá un mensaje con el resultado del servicio. Además, en el directorio en donde se clona el proyecto, se generará el Log correspondiente
al momento de ejecutar un WebService.


Saludos Cordiales
Juan Pablo Neira Valenzuela
Software Developer
